document.getElementById('check-price').addEventListener('click', function() {
  const symbol = document.getElementById('stock-symbol').value;
  fetch(`https://api.stockpricechecker.com/price?symbol=${symbol}`)
    .then(response => response.json())
    .then(data => {
      document.getElementById('price').textContent = `The latest price for ${symbol} is $${data.price}`;
    })
    .catch(error => {
      console.error('Error:', error);
    });
});
